FROM node:latest

MAINTAINER Evan Katsuranis "egaktsuranis@udavis.edu"

RUN npm install --silent -g webpack  && \
    mkdir -p /srv

COPY * /srv/

RUN cd /srv && \
    npm install --silent

EXPOSE 3000
CMD ["node", "/srv/server.js"]