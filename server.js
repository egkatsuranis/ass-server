var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, {origins:'*:*'});

var games = [];

function getNewRound() {
    return {

        roundFinished: false,
        result: '',
        basePpe: .8 + Math.random() * .2,
        baseAuthorize: Math.random() * .35,
        turns: [
        ]
    };
}

function givePlayerPoints(players, playerName, points) {
    players.forEach(function(player){
        if (playerName == player.name) {
            player.score += points;
        }
    });
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex ;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

// TODO remove disconnected sockets from game

io.on('connection', function(socket){
    console.log('a user connected');

    var userName;
    var userPicture;
    var gameBeingPlayed = -1;

    socket.on('registerPlayer', function(player){
        console.log('Registered %s', player.name);
        console.log('%s provided picture? %b', player.name, player.picture != '');
        userName = player.name;
        userPicture = player.picture;
        socket.emit('registeredPlayer', player);
    });

    socket.on('createGame', function(){
        console.log('%s creating game', userName);
        games.push({index: games.length, players: [], history: []});
        socket.broadcast.emit('gamesUpdated', games);
        socket.emit('gamesUpdated', games);
    });

    socket.on('getGames', function(){
        console.log('%s getting games', userName);
        socket.broadcast.emit('gamesUpdated', games);
        socket.emit('gamesUpdated', games);
    });

    socket.on('joinGame', function(gameIndex){

        var game = games[gameIndex];

        if (gameBeingPlayed > -1 || game.players.length > 2) {
            socket.emit('cannotJoinGame', games);
            console.log('%s cant join game %d already in game %d', userName, gameIndex, gameBeingPlayed);
            return;
        }

        gameBeingPlayed = gameIndex;

        game.players.push({name: userName, picture: userPicture, score: 0});
        socket.join(gameBeingPlayed.toString());

        if (game.players.length == 3) {
            game.history.push(getNewRound());
            game.players = shuffle(game.players);
        }

        console.log('%s joined game %d', userName, gameIndex);
        socket.emit('joinedGame', games);
        io.to(gameBeingPlayed.toString()).emit('updatedGame', game);
    });

    socket.on('getGame', function(){
        console.log(gameBeingPlayed);
        if (gameBeingPlayed < 0) {
            socket.emit('notInGame', games);
            return;
        }

        var game = games[gameBeingPlayed];
        console.log('%s getting game', userName);
        io.to(gameBeingPlayed.toString()).emit('updatedGame', games[gameBeingPlayed]);
    });

    socket.on('takeTurn', function(action){

        if (gameBeingPlayed < 0) {
            console.log('%s player cannot take turn, not in game');
            return;
        }

        // TODO check it's actually that person's turn :)

        var game = games[gameBeingPlayed];
        var roll = Math.random();
        var roundNumber = game.history.length;
        var round = game.history[roundNumber - 1];
        var points;
        var ppeGiven;

        console.log('player %s takes action %s on game %d', userName, action, gameBeingPlayed);

        var currentTurn = {
            name: userName,
            action: action,
            success: false
        };

        // TODO need to stop failed ppe from counting

        if (action == 'PPE') {
            if (roll < round.basePpe) {
                console.log('player %s added PPE', userName);
                currentTurn.success = true;
                round.baseAuthorize = round.baseAuthorize + 0.10;
                givePlayerPoints(game.players, userName, 10);
            } else {
                console.log('player %s failed to add PPE', userName);
                currentTurn.success = false;
                round.basePpe = round.basePpe - .10;
            }

            if (round.basePpe < 0) {
                round.roundFinished = true;
                round.result = 'BUREAUCRATS WERE TOO COWARDLY!!!';
                if (roundNumber < 5) {
                    game.history.push(getNewRound());
                }
            }

            round.basePpe = round.basePpe - 0.10;

        } else if (action =='AUTHORIZE') {
            if (roll < round.baseAuthorize) {
                console.log('player %s authorized a successful attempt', userName);
                currentTurn.success = true;
                round.roundFinished = true;
                round.result = 'HOBBITS DEFEATED MAD SCIENTIST';
                points = 10 + (1 - round.baseAuthorize) * 100;
                givePlayerPoints(game.players, userName, points);
                ppeGiven = 0;
                game.players.forEach(function(player) {
                    round.turns.forEach(function(turn){
                        if (player.name == turn.name && turn.action == 'PPE' && currentTurn.success) {
                            ppeGiven++;
                        }
                    });
                    givePlayerPoints(game.players, userName, ppeGiven * (points / 3) );
                });
            } else {
                console.log('player %s authorized a failed attempt', userName);
                round.roundFinished = true;
                round.result = 'HOBBITS LOST TO MAD SCIENTIST';
                points = 10 + Math.round((1 - round.baseAuthorize) * 100);
                givePlayerPoints(game.players, userName, -1 * points);
                ppeGiven = 0;
                game.players.forEach(function(player) {
                    round.turns.forEach(function(turn){
                        if (player.name == turn.name && turn.action == 'PPE' && currentTurn.success) {
                            ppeGiven++;
                        }
                    });
                    givePlayerPoints(game.players, userName, -1 * ppeGiven * (points / 3) );
                });
            }
        }

        round.turns.push(currentTurn);

        if (round.roundFinished && roundNumber < 5) {
            game.history.push(getNewRound());
        }

        game.players.forEach(function(player){
           player.score = Math.round(player.score);
        });

        console.log('game state is ', game);
        io.to(gameBeingPlayed.toString()).emit('updatedGame', game);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});